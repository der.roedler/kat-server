SHELL = /bin/bash

.PHONY: help docker-build docker-pull docker-up docker-stop docker-rm docker-backup

# Set directory of this Makefile to a variable to use later.
MAKEPATH := $(abspath $(lastword $(MAKEFILE_LIST)))
PWD := $(dir $(MAKEPATH))

# This will output the help for each task.
# Thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html.
help: ## This help.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

# Set help target as default target
.DEFAULT_GOAL := help

docker-pull: ## Pulls the Docker images.
	docker-compose pull

docker-up: ## Spins up the Docker containers.
	mkdir -p ./docker/volumes/database/{init,data}
	docker-compose up -d

docker-stop: ## Stops running Docker containers.
	docker-compose stop

docker-rm: docker-stop ## Stops and removes Docker containers.
	docker-compose rm

docker-backup: docker-stop ## Stops running Docker containers and makes a backup of the Docker Data Volumes.
	tar cfvz backup.tar.gz ./docker/volumes/
